# SAE Admin Semaine5 

Ce dépôt contient les fichiers nécessaires pour configurer un serveur avec Ansible.

## Contenu du dépôt

- `README.md`:  Ce fichier, qui fournit une description du contenu du dépôt.
- `invetaire.ini`: Fichier d'inventaire Ansible contenant les informations sur le serveur. 
- `playbook.yml`: Playbook Ansible pour configurer le serveur.

## Fonctionnement
Le playbook contient les tâches, pour configurer un serveur, suivantes:

1. Ajouter un groupe 
2. Ajouter un utilisateur au groupe 
3. Créer un fichier template
